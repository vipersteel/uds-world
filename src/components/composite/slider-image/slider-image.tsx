import React from 'react'
import style from './style.css'

interface SliderImageProps {
  photo: string;
}

const SliderImage: React.FC<SliderImageProps> = ({photo}) => {
  return (
    <div className={style.sliderContainer}>
      <div className={style.sliderImage}>
        <img src={photo} alt="slider-photo"/>
      </div>
    </div>
  )
}

export default SliderImage
