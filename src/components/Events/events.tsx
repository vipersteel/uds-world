import React from 'react'
import style from './style.css'


const Events = () => (
    <div>
        <p className={style.label}>Соревнования</p>
        <div className={style.line}></div>
        <div className={style.date}>
            <div className={style.event_left}>
                <p className={style.p_date}>12</p>
                <p className={style.p_month}>декабря</p>
                <div className={style.circle}></div>
                <p className={style.p_event}>Кубок Ренессанса 2018</p>
            </div>
            <div className={style.event}>
                <p className={style.p_date}>14</p>
                <p className={style.p_month}>декабря</p>
                <div className={style.circle}></div>
                <p className={style.p_event}>Муниципальное соревнование</p>
            </div>
            <div className={style.event}>
                <p className={style.p_date}>22</p>
                <p className={style.p_month}>декабря</p>
                <div className={style.circle}></div>
                <p className={style.p_event}>Кубок «Звездного вальса»</p>
            </div>
            <div className={style.event_right}>
                <p className={style.p_date}>11</p>
                <p className={style.p_month}>января</p>
                <div className={style.circle}></div>
                <p className={style.p_event}>Танцевальный марафон</p>
            </div>
        </div>
    </div>
)

export default Events