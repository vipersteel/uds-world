import { configureStore } from '@reduxjs/toolkit';

// @ts-ignore
import reducer from '../reducers';

export const store = configureStore({ reducer });
