import React from 'react'
import cn from 'classnames'
import style from './style.css'
import ArrowSvg from '../arrow-svg/Arrow-svg'


interface ButtonProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    onClick?: () => void;
    className?: string;
    type?: any;
    styleArrow?: string;
}


/*
Компонент кнопка со стрелкой, используется так же в других компонентах, чтобы повернуть вверх применить 
style.btn_up
Чтобы повернуть вниз
style.btn_down
Вправо
style.btn_right
Влево
style.btn_left
*/

const ButtonArrow: React.FC<ButtonProps> = ({className,styleArrow='right', ...rest  }) => {
    const styleArr = `btn_${styleArrow}`;
    return (
            <button
                {...rest}
                className={cn(className, style[styleArr])}
            >
                <ArrowSvg />
            </button>
    )
}


ButtonArrow.defaultProps = {

}

export default ButtonArrow;
