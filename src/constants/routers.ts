export const ROUTES = {
    MAIN: '/uds-world',                              // Главная
    INSTITUTIONS: '/institutions',          // Учереждения
    NEWS: '/news',                          // Новости
    SECTIONS: '/sections',                  // Секции
    RATINGS: '/ratings',                    // Рейтинги
    ERROR: '/error'                         // Внутрення ошибка
}
