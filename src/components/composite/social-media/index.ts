import SocialMedia from './social-media'
import { SocialMediaColor } from './social-media'

export default SocialMedia

export {
  SocialMediaColor
}
