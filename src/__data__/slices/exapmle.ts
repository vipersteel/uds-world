import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    data: null,
}

const slice = createSlice({
    name: 'example',
    initialState,
    reducers: {}
})

export const reducer = slice.reducer;
