import { string } from 'prop-types'
import React from 'react'

import style from './style.css'
interface ArrowSvgProps extends React.DetailedHTMLProps<React.SVGAttributes<HTMLOrSVGElement>, HTMLOrSVGElement> {
    d?:string;
    fill?: string;
}


const ArrowSvg: React.FC<ArrowSvgProps> = ({className,d, fill, ...rest  }) => (
    <span className={style.span}>
        <svg className={style.arrow}>
           <path d="M 8.66315 8.60947 C 8.86526 8.40737 8.99999 8.10421 8.99999 7.80105 C 8.99999 7.49789 8.86526 7.19474 8.66315 6.99263 L 2.49894 0.828421 C 2.06105 0.390526 1.31999 0.390526 0.8821 0.828421 C 0.444205 1.26632 0.444205 2.00737 0.8821 2.44526 L 6.20421 7.80105 L 0.8821 13.1232 C 0.64631 13.3589 0.545258 13.6284 0.545258 13.9316 C 0.545258 14.2347 0.64631 14.5379 0.8821 14.74 C 1.31999 15.1779 2.06105 15.1779 2.49894 14.74 L 8.66315 8.60947 Z"
           fill = {fill}
           ></path>
        </svg>
        </span>
)

ArrowSvg.defaultProps = {
    fill : "currentColor"
}

export default ArrowSvg;