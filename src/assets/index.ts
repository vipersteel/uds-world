import Vk from './icons/vk.svg'
import Instagram from './icons/instagram.svg'
import Odnoklassniki from './icons/odnoklassniki.svg'
import Facebook from './icons/facebook.svg'
import Twitter from './icons/twitter.svg'
import VkGray from './icons/vk-gray.svg'
import InstagramGray from './icons/instagram-gray.svg'
import OdnoklassnikiGray from './icons/odnoklassniki-gray.svg'
import FacebookGray from './icons/facebook-gray.svg'
import TwitterGray from './icons/twitter-gray.svg'
import LogoColored from './icons/logo-colored.svg'
import LogoGray from './icons/logo-gray.svg'
import photo from './images/photo.png'
import photo1 from './images/photo1.png'
import photo2 from './images/photo2.png'

export {
  Vk,
  Instagram,
  Odnoklassniki,
  Facebook,
  Twitter,
  VkGray,
  InstagramGray,
  OdnoklassnikiGray,
  FacebookGray,
  TwitterGray,
  LogoColored,
  LogoGray,
  photo,
  photo1,
  photo2,
}
