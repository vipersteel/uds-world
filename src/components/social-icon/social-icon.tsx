import React from 'react'
import style from './style.css'

export interface Icon {
  image: string,
  href: string
}

interface SocialIconProps {
  icon: Icon
}

const SocialIcon: React.FC<SocialIconProps> = ({icon}) => {
  
  return (
    <a className={style.link} href={icon.href} target="_blank" rel="noreferrer">
      <img src={icon.image} />
    </a>
  )
}

export default SocialIcon
