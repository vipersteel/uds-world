import React from 'react'
import { Facebook } from '../../../assets'
import { Instagram } from '../../../assets'
import { Odnoklassniki } from '../../../assets'
import { Twitter } from '../../../assets'
import { Vk } from '../../../assets'
import SocialIcon from '../../social-icon'
import { Icon } from '../../social-icon/social-icon'
import style from './style.css'
import PropTypes from 'prop-types'
import { FacebookGray } from '../../../assets'
import { VkGray } from '../../../assets'
import { InstagramGray } from '../../../assets'
import { OdnoklassnikiGray } from '../../../assets'
import { TwitterGray } from '../../../assets'

const links = {
  vk: 'https://vk.com',
  instagram: 'https://instagram.com',
  odnoklassniki: 'https://odnoklassniki.com',
  facebook: 'https://facebook.com',
  twitter: 'https://twitter.com',
}

const coloredIcons: Icon[] = [
  {image: Vk, href: links.vk},
  {image: Instagram, href: links.instagram},
  {image: Odnoklassniki, href: links.odnoklassniki},
  {image: Facebook, href: links.facebook},
  {image: Twitter, href: links.twitter},
]

const grayIcons: Icon[] = [
  {image: VkGray, href: links.vk},
  {image: InstagramGray, href: links.instagram},
  {image: OdnoklassnikiGray, href: links.odnoklassniki},
  {image: FacebookGray, href: links.facebook},
  {image: TwitterGray, href: links.twitter},
]

export enum SocialMediaColor {
  colored,
  gray,
}

interface SocialMediaProps {
  color: SocialMediaColor;
  title?: string;
}

const SocialMedia: React.FC<SocialMediaProps> = ({ color, title }) => {
  return (
    <div className={color === SocialMediaColor.colored ? style.wrapperForColored : style.wrapperForGray}>
        {title && color === SocialMediaColor.colored && <h3 className={style.title}>{title}</h3>}
        <div className={style.iconContainer}>
          {color === SocialMediaColor.colored && coloredIcons.map((icon, i) => <SocialIcon icon={icon} key={i} />)}
          {color === SocialMediaColor.gray && grayIcons.map((icon, i) => <SocialIcon icon={icon} key={i} />)}
        </div>
    </div>
  )
}

SocialMedia.propTypes = {
  color: PropTypes.oneOf([SocialMediaColor.colored, SocialMediaColor.gray]).isRequired,
  title: PropTypes.string,
}

export default SocialMedia
