import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import {Provider} from 'react-redux';

import { store } from './__data__/store';
import { ROUTES } from './constants/routers';
import { Main, Institutions, News, Sections, Ratings, Page404, PageError } from './pages';

const App = () => (
    <Provider store={store}>
        <BrowserRouter>
            <div>Header</div>
            <Switch>
                <Route exact path={ROUTES.MAIN} component={Main}/>
                <Route exact path={ROUTES.MAIN} component={Institutions}/>
                <Route exact path={ROUTES.MAIN} component={News}/>
                <Route exact path={ROUTES.MAIN} component={Sections}/>
                <Route exact path={ROUTES.MAIN} component={Ratings}/>
                <Route exact path={ROUTES.MAIN} component={PageError}/>
                <Route component={Page404}/>
            </Switch>
            <div>Footer</div>
            <div>Footer</div>
            <div>Footer</div>
            <div>Footer</div>
            <div>Footer</div>
            <div>Footer</div>
        </BrowserRouter>
    </Provider>
)

export default App;
