import React from 'react'
import { Link } from 'react-router-dom'
import { LogoColored } from '../../../assets'
import { LogoGray } from '../../../assets'
import style from './style.css'

export enum LogoColor {
  colored,
  gray,
}

interface LogoProps {
  color: LogoColor;
  to: string;
}

const Logo: React.FC<LogoProps> = ({ color, to }) => {
  return (
    <Link to={to} className={style.link}>
      {color === LogoColor.colored && <img src={LogoColored} alt="logotype"/>}
      {color === LogoColor.gray && <img src={LogoGray} alt="logotype"/>}
    </Link>
  )
}

export default Logo